from django.db import models


class ImageModel(models.Model):
    id = models.IntegerField(primary_key=True, null=False)
    pic = models.ImageField(upload_to='.', default='./img.png')

    def __str__(self):
        return str(self.id)
