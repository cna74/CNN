from django.urls import path
from . import views

app_name = 'cnn'

urlpatterns = [
    path('upload/', views.upload, name='upload'),
    path('list/', views.ImageModelList.as_view(), name='list'),
    path('media/<str:s>', views.img, name='img'),
]
