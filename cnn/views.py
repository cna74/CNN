from django.http import HttpResponse
from . import models
from . import predictor
from . import forms
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
from django.views.generic import ListView
import os
# Create your views here.


@csrf_exempt
def upload(request):
    if request.method == 'POST':
        img = forms.Img(request.POST, request.FILES)
        if img.is_valid():
            result = 'Error'
            m = models.ImageModel()
            m.pic = img.cleaned_data['image']
            m.save()
            dir_ = os.getcwd()
            result = predictor.detect('{}/media/{}'.format(dir_, m.pic.name))
            result = ''.join([' '.join([i, j])+'%\n\t' for i, j in result.items()])
            if result.strip() == '':
                result = 'Not found'
            return JsonResponse({'message': result})
        return JsonResponse({'message': 'invalid'})
    return JsonResponse({'message': 'POST image'})


def img(request, s):
    return HttpResponse('Meh :(')


class ImageModelList(ListView):
    model = models.ImageModel
