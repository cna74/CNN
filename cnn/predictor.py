# from keras.preprocessing.image import load_img
# from keras.preprocessing.image import img_to_array
# from keras.applications.vgg16 import preprocess_input
# from keras.applications.vgg16 import decode_predictions
# from keras.applications.vgg16 import VGG16
# from keras.backend.tensorflow_backend import clear_session
from clarifai.rest import ClarifaiApp


def detect(path: str):
    app = ClarifaiApp(api_key='8217e39b6b1c4f4cbbed8aed2197fc2e')
    model = app.models.get("food-items-v1.0")
    res = model.predict_by_filename(path)
    res = dict(res)
    res = res['outputs'][0]['data']['concepts']
    res = {i['name']: str(int(i['value']*100)) for i in res if i['name'] in ('apple', 'orange', 'banana') and i['value']>0.6}
    return res

    # clear_session()
    #
    # model = VGG16()
    #
    # image = load_img(path, target_size=(224, 224))
    # image = img_to_array(image)
    # image = image.reshape((1, image.shape[0], image.shape[1], image.shape[2]))
    # image = preprocess_input(image)
    #
    # y_hat = model.predict(image)
    #
    # label = decode_predictions(y_hat)
    # label = [(i[1], i[2]) for i in label[0][:5] if i[2] > 0.10 and i[1] in ('apple', 'orange', 'banana')]
    #
    # return ''.join([' '.join((i[0], str(int(i[1]*100))))+'%\n' for i in label])
